import pickle
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

def getModel():
    filename = 'finalized_model7.sav'

    loaded_model = pickle.load(open(filename, 'rb'))
    
    print("Model loaded "+loaded_model)

    return loaded_model;