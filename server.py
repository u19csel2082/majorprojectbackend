import pickle
import warnings

import numpy as np
# import socketioc
from flask import Flask, jsonify, request

warnings.simplefilter(action='ignore', category=FutureWarning)
import os
import random
from datetime import datetime
from os.path import dirname, join

import certifi
from dotenv import load_dotenv
from pymongo import MongoClient
from werkzeug.utils import secure_filename

# from gevent.pywsgi import WSGIServer
# from flask_socketio import SocketIO


dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

def get_database():
   ca = certifi.where()
   
   # Provide the mongodb atlas url to connect python to mongodb using pymongo
   CONNECTION_STRING = os.environ.get("CONNECTION")
   # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
   client = MongoClient(CONNECTION_STRING)
 
   # Create the database for our example (we will use the same database throughout the tutorial
   return client['major_project2']


os.environ["TOKENIZERS_PARALLELISM"] = "false"

UPLOAD_FOLDER = ('static/uploads')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
dbname = get_database()
print(pickle.format_version)
collection_name = dbname["posts"]
filename = 'finalized_model7.sav'
loaded_model = pickle.load(open(filename, 'rb'))


@app.route('/predict',methods=['POST'])
def prediction():
    data = request.form['exp']
    print("Result "+str(np.array(data)))
    predictions, raw_outputs = loaded_model.predict([str(np.array(data))])
    print("The answer is :"+str(predictions[0]))
    return jsonify({
        "success": True,
        "data":str(predictions[0]),
        "message": "Hate Speech"if str(predictions[0])=="1" else "Not Hate Speech",
    });

@app.route("/insertPost",methods=["POST"])
def insertData():
    data = request.form
    data_com = request.form['caption']
    predictions, raw_outputs = loaded_model.predict([str(np.array(data_com))])
    if(predictions[0]==1):
        return jsonify(
           {
            "success":False,
            "message":"Ooops! Looks like your caption violates our terms and conditions. Please be respectful and follow our community guidelines",
           }
        )
    data_com = request.form['title']
    predictions, raw_outputs = loaded_model.predict([str(np.array(data_com))])
    if(predictions[0]==1):
        return jsonify(
           {
            "success":False,
            "message":"Ooops! Looks like your title violates our terms and conditions. Please be respectful and follow our community guidelines",
           }
        )
    uploaded_img=request.files['post_image']
    img_filename = secure_filename(uploaded_img.filename)
    uploaded_img.save(os.path.join(app.config['UPLOAD_FOLDER'], img_filename))
    post_1={
        "id": random.randint(0,9999999),
        "image": os.path.join(app.config['UPLOAD_FOLDER'], img_filename),
        "title": data.get("title"),
        "username":data.get("username"),
        "caption": data.get("caption"),
        "comment":[],
        "created_date": str(datetime.now()),
        "updated_date": str(datetime.now()),
    }
    collection_name.insert_one(post_1);
    return jsonify({
        "success":True,
        "message":"Inserted successfully"
    });


@app.route("/posts",methods=["GET"])
def get_posts():
    items = collection_name.find({}, {'_id': False})
    return jsonify(
        {
            "success":True,
            "message":"Successfully retrieved",
            "data":list(items),
        }
    )

@app.route("/addComment",methods=["POST"])
def add_comment():
    print(request.get_json(force=True))
    print(request.get_json(force=True)['comment'])
    data = request.get_json(force=True)
    data_com = request.get_json(force=True)['comment']
    print("Result "+str(np.array(data_com)))
    predictions, raw_outputs = loaded_model.predict([str(np.array(data_com))])
    print("The answer is :"+str(predictions[0]))
    if(predictions[0]==1):
        return jsonify(
           {
            "success":False,
            "message":"Ooops! Looks like your comment violates our terms and conditions. Please be respectful and follow our community guidelines",
           }
        )
    comment={
        "id": random.randint(0,9999999),
        "comment": data['comment'],
        "created_date": str(datetime.now()),
        "updated_date": str(datetime.now()),
    }
    collection_name.update_one({"id":int(data['id'])},{
       "$push":{
        "comment": comment,
        },
       "$set":{
        "updated_date": str(datetime.now()),
        },
    })
    items = collection_name.find({}, {'_id': False})
    return jsonify(
        {
            "success":True,
            "message":"Successfully retrieved",
            "data":list(items),
        }
    )

if __name__ == "__main__":
    app.run(debug=True)
# if __name__ == '__main__':
#     # app.run(port=5000, debug=True)
#     # http_server = WSGIServer(('', 5000), app)
#     # http_server.serve_forever()
#     port = int(os.environ.get('PORT', 5000))
#     socketio.run(app, host='0.0.0.0', port=port,)
