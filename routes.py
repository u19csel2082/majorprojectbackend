from flask import Flask,Blueprint,jsonify,request
from extensions import get_database
import numpy as np
import os
from werkzeug.utils import secure_filename
import random
from datetime import datetime
# from model import getModel
import pickle
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# main = Blueprint('main',__name__)
os.environ["TOKENIZERS_PARALLELISM"] = "false"
UPLOAD_FOLDER = ('static/uploads')
main = Blueprint('main',__name__,UPLOAD_FOLDER, None)



# main.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
dbname = get_database()
filename = 'finalized_model7.sav'
loaded_model = pickle.load(open(filename, 'rb'))
collection_name = dbname["posts"]

@main.route("/insertPost",methods=["POST"])
def insertData():
    data = request.form
    data_com = request.form['caption']
    filename = 'finalized_model7.sav'
    loaded_model = pickle.load(open(filename, 'rb'))
    predictions, raw_outputs = loaded_model.predict([str(np.array(data_com))])
    if(predictions[0]==1):
        return jsonify(
           {
            "success":False,
            "message":"Ooops! Looks like your caption violates our terms and conditions. Please be respectful and follow our community guidelines",
           }
        )
    data_com = request.form['title']
    predictions, raw_outputs = loaded_model.predict([str(np.array(data_com))])
    if(predictions[0]==1):
        return jsonify(
           {
            "success":False,
            "message":"Ooops! Looks like your title violates our terms and conditions. Please be respectful and follow our community guidelines",
           }
        )
    uploaded_img=request.files['post_image']
    img_filename = secure_filename(uploaded_img.filename)
    uploaded_img.save(os.path.join(main.static_folder, img_filename))
    post_1={
        "id": random.randint(0,9999999),
        "image": os.path.join(UPLOAD_FOLDER, img_filename),
        "title": data.get("title"),
        "username":data.get("username"),
        "caption": data.get("caption"),
        "comment":[],
        "created_date": str(datetime.now()),
        "updated_date": str(datetime.now()),
    }
    collection_name.insert_one(post_1);
    return jsonify({ 
        "success":True,
        "message":"Inserted successfully"
    });


@main.route("/posts",methods=["GET"])
def get_posts():
    items = collection_name.find({}, {'_id': False})
    return jsonify(
        {
            "success":True,
            "message":"Successfully retrieved",
            "data":list(items),
        }
    )

@main.route("/addComment",methods=["POST"])
def add_comment():
    print(request.get_json(force=True))
    print(request.get_json(force=True)['comment'])
    data = request.get_json(force=True)
    data_com = request.get_json(force=True)['comment']
    print("Result "+str(np.array(data_com)))
    predictions, raw_outputs = loaded_model.predict([str(np.array(data_com))])
    print("The answer is :"+str(predictions[0]))
    if(predictions[0]==1):
        return jsonify(
           {
            "success":False,
            "message":"Ooops! Looks like your comment violates our terms and conditions. Please be respectful and follow our community guidelines",
           }
        )
    comment={
        "id": random.randint(0,9999999),
        "comment": data['comment'],
        "created_date": str(datetime.now()),
        "updated_date": str(datetime.now()),
    }
    collection_name.update_one({"id":int(data['id'])},{
       "$push":{
        "comment": comment,
        },
       "$set":{
        "updated_date": str(datetime.now()),
        },
    })
    items = collection_name.find({}, {'_id': False})
    return jsonify(
        {
            "success":True,
            "message":"Successfully retrieved",
            "data":list(items),
        }
    )
